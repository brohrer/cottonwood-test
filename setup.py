import setuptools as st

st.setup(
    name='cottonwood_test',
    version='32',
    description='A test suite for Cottonwood',
    url='http://github.com/brohrer/cottonwood-test',
    download_url='https://gitlab.com/brohrer/cottonwood-test/-/archive/main/cottonwood-test-main.zip',
    author='Brandon Rohrer',
    author_email='brohrer@gmail.com',
    license='MIT',
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    python_requires='>=3.6',
)
