import os
import numpy as np
from cottonwood.activation import Logistic, ReLU, TanH
from cottonwood.initialization import Glorot, He, LSUV
from cottonwood.linear import Linear
from cottonwood.loggers import ValueLogger
from cottonwood.loss import AbsoluteLoss
from cottonwood.normalization import Bias, MinMaxNormalization
from cottonwood.operations import Copy, Difference, Flatten
from cottonwood.optimization import Adam, Momentum, SGD
from cottonwood.structure import Structure
import cottonwood.toolbox as tb
from cottonwood.data.nordic_runes import EvaluationData, TrainingData
import cottonwood.experimental.visualize_structure as viz


def run():
    """
    A functional test some important parts of Cottonwood, including
        initializers Glorot, He, and LSUV
        optimizers Adam, Momentum, and SGD
        activation functions Logistic, ReLU, and TanH
        AbsoluteLoss

    Don't worry about performance on this one. It's a Frankenstein network
    built for the sole purpose of testing a lot of components. As long
    as it runs without crashing and
    the result is mostly smooth and shows a slight dip, it gets a passing
    grade.
    """
    reports_dir = os.path.join("reports", "test_01")
    os.makedirs(reports_dir, exist_ok=True)

    msg = f"""
Testing a Structure including LinearRegularized, Logistic, and ReLU blocks.
  Look for documentation and visualizations
  in the {reports_dir} directory.
"""
    print(msg)

    # Build the structure for the training runs
    n_training_iter = 1000
    n_evaluation_iter = 1000
    n_report_interval = 100
    reporting_bin_size = 10
    n_bottleneck_nodes = 24

    autoencoder = Structure()
    autoencoder.add(TrainingData(), "train")
    sample = autoencoder.blocks["train"].forward_pass()
    n_pixels = np.prod(sample.shape)

    autoencoder.add(Flatten(), "flatten")
    autoencoder.add(MinMaxNormalization(norm_min=0, norm_max=1), "norm_0")
    autoencoder.add(Copy(), "copy")
    autoencoder.add(Linear(
        n_bottleneck_nodes,
        l1_param=.0001,
        l2_param=.001,
        initializer=He(),
        optimizer=SGD(),
    ), "lin_0")
    autoencoder.add(Bias(), "bias_0")
    autoencoder.add(ReLU(), "act_0")
    autoencoder.add(MinMaxNormalization(norm_min=-.5, norm_max=.5), "norm_1")
    autoencoder.add(Linear(
        n_bottleneck_nodes,
        l1_param=.00005,
        l1_threshold=.001,
        l2_param=.0003,
        initializer=Glorot(),
        optimizer=Adam(),
    ), "lin_1")
    autoencoder.add(Bias(), "bias_1")
    autoencoder.add(TanH(), "act_1")
    autoencoder.add(MinMaxNormalization(norm_min=0, norm_max=1), "norm_2")
    autoencoder.add(Linear(
        n_pixels,
        initializer=LSUV(),
        optimizer=Momentum(),
    ), "lin_2")
    autoencoder.add(Bias(), "bias_2")
    autoencoder.add(Logistic(), "act_2")
    autoencoder.add(Difference(), "diff")
    autoencoder.add(AbsoluteLoss(), "abs_loss")

    autoencoder.connect_sequence([
        "train",
        "flatten",
        "norm_0",
        "copy",
        "lin_0",
        "bias_0",
        "act_0",
        "norm_1",
        "lin_1",
        "bias_1",
        "act_1",
        "norm_2",
        "lin_2",
        "bias_2",
        "act_2",
        "diff",
        "abs_loss",
    ])

    autoencoder.connect(
        tail_block="copy",
        i_port_tail=1,
        head_block="diff",
        i_port_head=1)

    # viz.render(autoencoder, reports_dir=reports_dir)
    loss_logger = ValueLogger(
        value_name="loss",
        log_scale=False,
        n_iter_report=n_report_interval,
        report_min=-1,
        report_max=0,
        reports_dir=reports_dir,
        reporting_bin_size=reporting_bin_size,
    )

    for i_iter in range(n_training_iter):
        result = autoencoder.forward_pass()
        autoencoder.backward_pass(result)
        loss_logger.log_value(autoencoder.blocks["abs_loss"].forward_out)

    tb.summarize(autoencoder, reports_dir=reports_dir)

    # Modify the structure to complete the evaluation runs.
    # Remove the training data block and replace
    # with the evaluation data block.
    autoencoder.remove("train")
    autoencoder.add(EvaluationData(), "eval")
    autoencoder.connect(tail_block="eval", head_block="flatten")

    for i_iter in range(n_evaluation_iter):
        result = autoencoder.forward_pass()
        loss_logger.log_value(autoencoder.blocks["abs_loss"].forward_out)


if __name__ == "__main__":
    run()
