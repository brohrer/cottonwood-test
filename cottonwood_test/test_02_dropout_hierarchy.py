import os
import numpy as np
from cottonwood.activation import TanH
from cottonwood.dropout import Dropout
from cottonwood.linear import Linear
from cottonwood.loggers import ValueLogger
from cottonwood.loss import SquareLoss
from cottonwood.normalization import Bias, MinMaxNormalization
from cottonwood.operations import Copy
from cottonwood.optimization import Momentum
from cottonwood.structure import Structure
import cottonwood.toolbox as tb
from cottonwood.data.nordic_runes import EvaluationData, TrainingData
import cottonwood.experimental.visualize_structure as viz


def run():
    """
    A functional test some important parts of Cottonwood, including
    Structure, ValueLogger, and several blocks:
        Linear, TanH
        SquareLoss
        MinMaxNormalization
        Copy, Difference

    This test is based on the Nordic runes demo in
    examples/autoencoder
    """
    reports_dir = os.path.join("reports", "test_02")
    os.makedirs(reports_dir, exist_ok=True)

    msg = f"""
Testing a Structure including Linear and TanH blocks.
  Look for documentation and visualizations
  in the {reports_dir} directory.
"""
    print(msg)

    # Build the structure for the training runs
    n_training_iter = int(1e4)
    n_evaluation_iter = int(1e4)
    n_report_interval = int(1e3)
    reporting_bin_size = int(1e2)
    n_bottleneck_nodes = 24

    autoencoder = Structure()
    autoencoder.add(TrainingData(), "train")
    sample = autoencoder.blocks["train"].forward_pass()
    n_pixels = np.prod(sample.shape)

    # Build the two dense layers separately before combining into
    # the final autoencoder
    dense_0 = Structure()
    dense_0.add(Linear(
        n_bottleneck_nodes,
        optimizer=Momentum(learning_rate=1e-7),
    ), "lin")
    dense_0.add(Bias(), "bias")
    dense_0.add(TanH(), "tanh")
    dense_0.add(Dropout(), "dropout")
    dense_0.connect_sequence([
        "lin",
        "bias",
        "tanh",
        "dropout",
    ])

    dense_1 = Structure()
    dense_1.add(Linear(
        n_pixels,
        optimizer=Momentum(learning_rate=1e-7),
    ), "lin_1")
    dense_1.add(Bias(), "bias_1")
    dense_1.add(TanH(), "tanh_1")
    dense_1.connect_sequence([
        "lin_1",
        "bias_1",
        "tanh_1",
    ])

    autoencoder.add(MinMaxNormalization(), "norm")
    autoencoder.add(Copy(), "copy")
    autoencoder.add(dense_0, "dense_0")
    autoencoder.add(dense_1, "dense_1")
    # autoencoder.add(AbsoluteLoss(), "loss")
    autoencoder.add(SquareLoss(), "loss")

    autoencoder.connect_sequence([
        "train",
        "norm",
        "copy",
        "dense_0",
        "dense_1",
        "loss",
    ])
    autoencoder.connect(
        tail_block="copy",
        i_port_tail=1,
        head_block="loss",
        i_port_head=1)

    loss_logger = ValueLogger(
        value_name="loss",
        # log_scale=True,
        n_iter_report=n_report_interval,
        report_min=-3,
        report_max=0,
        reports_dir=reports_dir,
        reporting_bin_size=reporting_bin_size,
    )

    for i_iter in range(n_training_iter):
        autoencoder.forward_pass()
        autoencoder.backward_pass()
        loss_logger.log_value(autoencoder.blocks["loss"].forward_out)

    # Modify the structure to complete the evaluation runs.
    # Remove the training data block and replace
    # with the evaluation data block.
    autoencoder.remove("train")
    autoencoder.add(EvaluationData(), "eval")
    autoencoder.connect(tail_block="eval", head_block="norm")

    for i_iter in range(n_evaluation_iter):
        autoencoder.forward_pass()
        loss_logger.log_value(autoencoder.blocks["loss"].forward_out)

    tb.summarize(autoencoder, reports_dir=reports_dir)
    # viz.render(autoencoder, reports_dir=reports_dir)


if __name__ == "__main__":
    run()
