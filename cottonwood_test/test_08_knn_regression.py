import os
import numpy as np
import matplotlib.pyplot as plt
from cottonwood.structure import Structure
from cottonwood.knn import KNN
try:
    from cottonwood_data_diamonds.diamonds_block import Data
    diamonds_loaded = True
except Exception:
    diamonds_loaded = False


def run():
    """
    A functional test some important parts of Cottonwood, including KNN
    This test is based on the diamonds data set study
    https://gitlab.com/brohrer/study-knn-diamonds
    """
    if not diamonds_loaded:
        print("The cottonwood_data_diamonds package didn't load correctly.")
        print("Try")
        print("$ git clone https://gitlab.com/brohrer/cottonwood-data-diamonds")
        print("$ python3 -m pip install -e cottonwood-data-diamonds")
        return

    reports_dir = os.path.join("reports", "test_08")
    os.makedirs(reports_dir, exist_ok=True)
    figname = "learning_curve.png"
    figpath = os.path.join(reports_dir, figname)

    msg = f"""
Testing the KNN block.
  Look for documentation and visualizations
  in the {reports_dir} directory.
"""
    print(msg)

    k = 7
    n_bin = 10

    # Create the k-NN model in Cottonwood.
    model = Structure()

    # Initialize the blocks.
    model.add(Data(data_filename="diamonds_sample.csv"), "data")
    model.add(KNN(
        k=k,
        is_classifier=False,
        max_points=3e4,
        use_pretrained_weights=True,
    ), "knn")

    # Hook them up together.
    model.connect("data", "knn")
    model.connect("data", "knn", i_port_tail=1, i_port_head=1)

    differences = []
    for i_iter in range(int(model.blocks["data"].n_examples)):
        model.forward_pass()
        try:
            differences.append(np.abs(
                model.blocks["knn"].target_label -
                model.blocks["knn"].forward_out))
        except TypeError:
            # Initially some of the estimates are None. Ignore these
            pass

    differences = np.array(differences[k + 1:])
    differences = differences[:(differences.size // n_bin) * n_bin]
    differences = np.reshape(differences, (n_bin, -1), order="F")
    mean_differences = np.mean(differences, axis=0)
    pct = (10 ** mean_differences - 1) * 100

    iters = np.arange(mean_differences.size) * n_bin
    fig = plt.figure()
    ax = fig.gca()
    ax.plot(iters, pct, color="#04253a")
    ax.set_title(f"k-NN with Diamond Prices, k = {model.blocks['knn'].k}")
    ax.set_xlabel("Iteration")
    ax.set_ylabel("Mean percentage price error")
    ax.grid()
    plt.savefig(figpath, dpi=300)


if __name__ == "__main__":
    run()
