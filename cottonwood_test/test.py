try:
    import matplotlib.pyplot as plt  # noqa F401
except ImportError:
    print("""

======================================================================
Couldn't import Matplotlib?
If you're working on Windows 10, it could be due to a missing package.
At the command line run

    python -m pip install -U  msvc-runtime

to remedy the situation. For more backrgound:
https://github.com/matplotlib/matplotlib/issues/14558
======================================================================

""")

import cottonwood_test.test_00_linear_tanh_structure as test_00
import cottonwood_test.test_01_activation_initializers_optimizers as test_01
import cottonwood_test.test_02_dropout_hierarchy as test_02
import cottonwood_test.test_03_bias_scale_normalization as test_03
import cottonwood_test.test_04_sparsify as test_04
import cottonwood_test.test_05_conv1d as test_05
import cottonwood_test.test_06_conv2d as test_06
import cottonwood_test.test_07_knn_classification as test_07
import cottonwood_test.test_08_knn_regression as test_08


test_00.run()
test_01.run()
test_02.run()
test_03.run()
test_04.run()
test_05.run()
test_06.run()
test_07.run()
test_08.run()
