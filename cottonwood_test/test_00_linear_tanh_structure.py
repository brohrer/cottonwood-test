import os
import numpy as np
from cottonwood.activation import TanH
from cottonwood.linear import Linear
from cottonwood.loggers import ValueLogger
from cottonwood.loss import SquareLoss
from cottonwood.normalization import Bias, MinMaxNormalization
from cottonwood.operations import Copy, Difference, Flatten
from cottonwood.structure import Structure
import cottonwood.toolbox as tb
from cottonwood.data.nordic_runes import EvaluationData, TrainingData
import cottonwood.experimental.visualize_structure as viz


def run():
    """
    A functional test some important parts of Cottonwood, including
    Structure, ValueLogger, and several blocks:
        Linear, TanH
        SquareLoss
        MinMaxNormalization
        Copy, Difference

    This test is based on the Nordic runes demo in
    examples/autoencoder
    """
    reports_dir = os.path.join("reports", "test_00")
    os.makedirs(reports_dir, exist_ok=True)

    msg = f"""
Testing a Structure including Linear and TanH blocks.
  Look for documentation and visualizations
  in the {reports_dir} directory.
"""
    print(msg)

    # Build the structure for the training runs
    n_training_iter = 1000
    n_evaluation_iter = 1000
    n_report_interval = 100
    reporting_bin_size = 10
    n_bottleneck_nodes = 24

    autoencoder = Structure()
    autoencoder.add(TrainingData(), "train")
    sample = autoencoder.blocks["train"].forward_pass()
    n_pixels = np.prod(sample.shape)

    autoencoder.add(Flatten(), "flatten")
    autoencoder.add(MinMaxNormalization(), "norm")
    autoencoder.add(Copy(), "copy")
    autoencoder.add(Linear(n_bottleneck_nodes), "lin_0")
    autoencoder.add(Bias(), "bias_0")
    autoencoder.add(TanH(), "tanh_0")
    autoencoder.add(Linear(n_pixels), "lin_1")
    autoencoder.add(Bias(), "bias_1")
    autoencoder.add(TanH(), "tanh_1")
    autoencoder.add(Difference(), "diff")
    autoencoder.add(SquareLoss(), "loss")

    autoencoder.connect_sequence([
        "train",
        "flatten",
        "norm",
        "copy",
        "lin_0",
        "bias_0",
        "tanh_0",
        "lin_1",
        "bias_1",
        "tanh_1",
        "diff",
        "loss",
    ])
    autoencoder.connect(
        tail_block="copy",
        i_port_tail=1,
        head_block="diff",
        i_port_head=1)

    # viz.render(autoencoder, reports_dir=reports_dir)
    loss_logger = ValueLogger(
        value_name="loss",
        log_scale=True,
        n_iter_report=n_report_interval,
        report_min=-3,
        report_max=0,
        reports_dir=reports_dir,
        reporting_bin_size=reporting_bin_size,
    )

    for i_iter in range(n_training_iter):
        autoencoder.forward_pass()
        autoencoder.backward_pass()
        loss_logger.log_value(autoencoder.blocks["loss"].forward_out)

    tb.summarize(autoencoder, reports_dir=reports_dir)

    # Modify the structure to complete the evaluation runs.
    # Remove the training data block and replace
    # with the evaluation data block.
    autoencoder.remove("train")
    autoencoder.add(EvaluationData(), "eval")
    autoencoder.connect(tail_block="eval", head_block="flatten")

    for i_iter in range(n_evaluation_iter):
        autoencoder.forward_pass()
        loss_logger.log_value(autoencoder.blocks["loss"].forward_out)


if __name__ == "__main__":
    run()
