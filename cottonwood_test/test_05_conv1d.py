import os
from cottonwood.activation import Logistic, ReLU
from cottonwood.conv1d import Conv1D
from cottonwood.linear import Linear
from cottonwood.loggers import ValueLogger
from cottonwood.loss import SquareLoss
from cottonwood.normalization import Bias
from cottonwood.operations import Difference, Flatten, OneHot
from cottonwood.pooling import MaxPool1D
from cottonwood.structure import Structure
import cottonwood.toolbox as tb
from cottonwood.data.blips import EvaluationData, TrainingData
from cottonwood.experimental.online_normalization_1d \
    import OnlineNormalization1D
import cottonwood.experimental.visualize_conv1d as conv_viz
import cottonwood.experimental.visualize_structure as struct_viz


def run():
    reports_dir = os.path.join("reports", "test_05")
    os.makedirs(reports_dir, exist_ok=True)

    msg = f"""
Testing one dimensional convolution and blocks
  for Max1DPool, OnlineNoramlization1D,
  Flatten, and OneHot operations.
  Look for documentation and visualizations
  in the {reports_dir} directory.

"""
    print(msg)

    n_training_iter = int(1e4)
    n_evaluation_iter = int(2e3)
    n_report_interval = int(1e3)
    n_viz_interval = int(1e5)

    convnet = Structure()
    convnet.add(TrainingData(), "train")
    convnet.add(OneHot(4), "onehot")
    kernel_size = [5, 3]
    n_kernels = [15, 12]

    # Create two convolutional layers of different sizes
    convnet.add(
        Conv1D(
            kernel_size=kernel_size[0],
            l1_param=.001,
            l1_threshold=.05,
            n_kernels=n_kernels[0]
        ), "conv_0")
    convnet.add(Bias(), "bias_0")
    convnet.add(ReLU(), "act_0")
    convnet.add(MaxPool1D(), "pool_0")
    convnet.add(OnlineNormalization1D(), "onorm_0")
    convnet.add(
        Conv1D(
            kernel_size=kernel_size[1],
            l2_param=.0001,
            n_kernels=n_kernels[1]
        ), "conv_1")
    convnet.add(Bias(), "bias_1")
    convnet.add(ReLU(), "act_1")
    convnet.add(MaxPool1D(), "pool_1")
    convnet.add(Flatten(), "flat")
    convnet.add(Linear(4), "lin_2")
    convnet.add(Bias(), "bias_2")
    convnet.add(Logistic(), "logit_2")
    convnet.add(Difference(), "diff")
    convnet.add(SquareLoss(), "sq_loss")

    convnet.connect_sequence([
        "train",
        "conv_0",
        "onorm_0",
        "bias_0",
        "act_0",
        "pool_0",
        "conv_1",
        "bias_1",
        "act_1",
        "pool_1",
        "flat",
        "lin_2",
        "bias_2",
        "logit_2",
        "diff",
        "sq_loss",
    ])
    convnet.connect(
        tail_block="train", i_port_tail=1, head_block="onehot", i_port_head=0)
    convnet.connect(
        tail_block="onehot", i_port_tail=0, head_block="diff", i_port_head=1)

    loss_logger = ValueLogger(
        value_name="loss",
        log_scale=True,
        n_iter_report=n_report_interval,
        report_min=-3.5,
        report_max=0,
        reports_dir=reports_dir,
        reporting_bin_size=1e3,
    )

    for i_iter in range(n_training_iter):
        convnet.forward_pass()
        convnet.backward_pass()
        loss_logger.log_value(convnet.blocks["sq_loss"].forward_out)
        if (i_iter + 1) % n_viz_interval == 0:
            conv_viz.render(
                convnet.blocks["conv_0"],
                reports_dir,
                f"conv_0_{i_iter + 1:07}")
            conv_viz.render(
                convnet.blocks["conv_1"],
                reports_dir,
                f"conv_1_{i_iter + 1:07}")

    tb.summarize(convnet, reports_dir=reports_dir)
    # struct_viz.render(convnet, reports_dir)

    convnet.remove("train")
    convnet.add(EvaluationData(), "eval")
    convnet.connect(
        tail_block="eval", i_port_tail=0, head_block="conv_0", i_port_head=0)
    convnet.connect(
        tail_block="eval", i_port_tail=1, head_block="onehot", i_port_head=0)

    for i_iter in range(n_evaluation_iter):
        convnet.forward_pass()
        loss_logger.log_value(convnet.blocks["sq_loss"].forward_out)
        if (i_iter + 1) % n_viz_interval == 0:
            conv_viz.render(
                convnet.blocks["conv_0"],
                reports_dir,
                f"conv_0_{n_training_iter + i_iter + 1:07}")
            conv_viz.render(
                convnet.blocks["conv_1"],
                reports_dir,
                f"conv_1_{n_training_iter + i_iter + 1:07}")


if __name__ == "__main__":
    run()
