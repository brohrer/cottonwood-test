import os
from cottonwood.activation import Logistic, TanH
from cottonwood.conv2d import Conv2D
from cottonwood.linear import Linear
from cottonwood.loggers import ValueLogger
from cottonwood.loss import SquareLoss
from cottonwood.normalization import Bias, MinMaxNormalization
from cottonwood.operations import Difference, Flatten, OneHot
from cottonwood.pooling import MaxPool2D
from cottonwood.structure import Structure
import cottonwood.toolbox as tb
import cottonwood.experimental.visualize_structure as struct_viz
from cottonwood.experimental.online_normalization_2d \
    import OnlineNormalization2D
try:
    from cottonwood_data_mnist.mnist_block import TestingData, TrainingData
    mnist_loaded = True
except Exception:
    mnist_loaded = False

N_ITER_TRAIN = int(1e3)
N_ITER_TEST = int(2e2)
N_ITER_REPORT = int(1e2)
REPORTS_DIR = os.path.join("reports", "test_06")


def run():
    if not mnist_loaded:
        print("The cottonwood-data-mnist package didn't load correctly.")
        print("Try")
        print("$ git clone https://gitlab.com/brohrer/cottonwood-data-mnist")
        print("$ python3 -m pip install -e cottonwood-data-mnist")
        return

    os.makedirs(REPORTS_DIR, exist_ok=True)

    msg = f"""
Testing two dimensional convolution and Max2DPool
  Look for documentation and visualizations
  in the {REPORTS_DIR} directory.

"""
    print(msg)
    model = Structure()
    model.add(TrainingData(), "training data")
    model.add(MinMaxNormalization(), "min-max norm")
    model.add(Conv2D(l1_param=1e-5, l1_threshold=1e-3), "conv_0")
    model.add(Bias(), "bias_0")
    model.add(TanH(), "tanh_0")
    model.add(MaxPool2D(), "pool_0")
    model.add(Conv2D(l2_param=1e-4), "conv_1")
    model.add(OnlineNormalization2D(), "onorm_1")
    model.add(Bias(), "bias_1")
    model.add(TanH(), "tanh_1")
    model.add(Flatten(), "flatten")
    model.add(Linear(10), "linear_2")
    model.add(Bias(), "bias_2")
    model.add(Logistic(), "logistic_2")

    model.add(OneHot(10), "one hot")
    model.add(Difference(), "difference")
    model.add(SquareLoss(), "square")

    model.connect_sequence([
        "training data",
        "min-max norm",
        "conv_0",
        "bias_0",
        "tanh_0",
        "pool_0",
        "conv_1",
        "onorm_1",
        "bias_1",
        "tanh_1",
        "flatten",
        "linear_2",
        "bias_2",
        "logistic_2",
        "difference",
        "square",
    ])

    model.connect("training data", "one hot", i_port_tail=1)
    model.connect("one hot", "difference", i_port_head=1)

    loss_logger = ValueLogger(
        n_iter_report=N_ITER_REPORT,
        reports_dir=REPORTS_DIR,
        value_name="loss")
    for i_iter in range(N_ITER_TRAIN):
        model.forward_pass()
        model.backward_pass()
        loss_logger.log_value(model.blocks["square"].forward_out)

    tb.summarize(model, reports_dir=REPORTS_DIR)
    # struct_viz.render(model, REPORTS_DIR)
    model.remove("training data")
    model.add(TestingData(), "testing data")
    model.connect("testing data", "min-max norm", i_port_tail=0)
    model.connect("testing data", "one hot", i_port_tail=1)

    for i_iter in range(N_ITER_TEST):
        model.forward_pass()
        loss_logger.log_value(model.blocks["square"].forward_out)


if __name__ == "__main__":
    run()
