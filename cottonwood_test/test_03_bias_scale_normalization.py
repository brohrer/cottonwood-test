import os
import numpy as np
from cottonwood.activation import Logistic, ReLU, TanH
from cottonwood.initialization import Glorot, He, LSUV
from cottonwood.linear import Linear
from cottonwood.loggers import ValueLogger
from cottonwood.loss import AbsoluteLoss
from cottonwood.normalization import \
    Bias, MinMaxNormalization, Scale
from cottonwood.operations import Copy
from cottonwood.optimization import Adam, Momentum, SGD
from cottonwood.structure import Structure
import cottonwood.toolbox as tb
from cottonwood.data.nordic_runes import EvaluationData, TrainingData
from cottonwood.experimental.online_normalization import \
   OnlineNormalization as Normalization
import cottonwood.experimental.visualize_structure as viz


def run():
    """
    A functional test some important parts of Cottonwood, including
        normalizers Bias, Scale, Normalization, and MinMaxNormalization

    Don't worry about performance on this one. It's a Frankenstein network
    built for the sole purpose of testing a lot of components. As long
    as it runs without crashing and
    the result is mostly smooth and shows a slight dip, it gets a passing
    grade.
    """
    reports_dir = os.path.join("reports", "test_03")
    os.makedirs(reports_dir, exist_ok=True)

    msg = f"""
Testing a Structure including Logistic, and ReLU blocks.
  Look for documentation and visualizations
  in the {reports_dir} directory.
"""
    print(msg)

    # Build the structure for the training runs
    n_training_iter = int(1e4)
    n_evaluation_iter = int(1e4)
    n_report_interval = int(1e3)
    reporting_bin_size = int(1e2)
    n_bottleneck_nodes = 24

    autoencoder = Structure()
    autoencoder.add(TrainingData(), "train")
    sample = autoencoder.blocks["train"].forward_pass()
    n_pixels = np.prod(sample.shape)
    learning_rate = 1e-6

    autoencoder.add(MinMaxNormalization(norm_min=0, norm_max=1), "norm_pre")
    autoencoder.add(Copy(), "copy")
    autoencoder.add(Linear(
        n_bottleneck_nodes,
        l1_param=.0001,
        l1_threshold=.001,
        l2_param=.001,
        initializer=He(),
        optimizer=SGD(learning_rate=learning_rate),
    ), "lin_0")
    autoencoder.add(Normalization(), "norm_0")
    autoencoder.add(Scale(
        optimizer=SGD(learning_rate=learning_rate),), "scale_0")
    autoencoder.add(Bias(
        optimizer=SGD(learning_rate=learning_rate),), "bias_0")
    autoencoder.add(ReLU(), "act_0")
    autoencoder.add(Linear(
        n_bottleneck_nodes,
        l1_param=.0005,
        l2_param=.003,
        initializer=Glorot(),
        optimizer=Adam(learning_rate=learning_rate),
    ), "lin_1")
    autoencoder.add(Scale(
        optimizer=Adam(learning_rate=learning_rate),), "scale_1")
    autoencoder.add(Bias(
        optimizer=Adam(learning_rate=learning_rate),), "bias_1")
    autoencoder.add(TanH(), "act_1")
    autoencoder.add(Linear(
        n_pixels,
        initializer=LSUV(),
        optimizer=Momentum(learning_rate=learning_rate),
    ), "lin_2")
    autoencoder.add(Scale(
        optimizer=Momentum(learning_rate=learning_rate),), "scale_2")
    autoencoder.add(Bias(
        optimizer=Momentum(learning_rate=learning_rate),), "bias_2")
    autoencoder.add(Logistic(), "act_2")
    autoencoder.add(AbsoluteLoss(), "abs_loss")

    autoencoder.connect_sequence([
        "train",
        "norm_pre",
        "copy",
        "lin_0",
        "norm_0",
        "scale_0",
        "bias_0",
        "act_0",
        "lin_1",
        "scale_1",
        "bias_1",
        "act_1",
        "lin_2",
        "scale_2",
        "bias_2",
        "act_2",
        "abs_loss",
    ])

    autoencoder.connect(
        tail_block="copy",
        i_port_tail=1,
        head_block="abs_loss",
        i_port_head=1)

    loss_logger = ValueLogger(
        value_name="loss",
        n_iter_report=n_report_interval,
        report_min=-1,
        report_max=0,
        reports_dir=reports_dir,
        reporting_bin_size=reporting_bin_size,
    )

    for i_iter in range(n_training_iter):
        result = autoencoder.forward_pass()
        autoencoder.backward_pass(result)
        loss_logger.log_value(autoencoder.blocks["abs_loss"].forward_out)

    # Modify the structure to complete the evaluation runs.
    # Remove the training data block and replace
    # with the evaluation data block.
    autoencoder.remove("train")
    autoencoder.add(EvaluationData(), "eval")
    autoencoder.connect(tail_block="eval", head_block="norm_pre")

    for i_iter in range(n_evaluation_iter):
        result = autoencoder.forward_pass()
        loss_logger.log_value(autoencoder.blocks["abs_loss"].forward_out)

    tb.summarize(autoencoder, reports_dir=reports_dir)
    # viz.render(autoencoder, reports_dir=reports_dir)


if __name__ == "__main__":
    run()
