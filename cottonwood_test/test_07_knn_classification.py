"""
This code makes use of the Cottonwood machine learning framework.
Get it at https://e2eml.school/cottonwood
"""
import os
import numpy as np
import matplotlib.pyplot as plt
from cottonwood.structure import Structure
from cottonwood.knn import KNN
from cottonwood.data.penguins import Data


def run():
    """
    A functional test some important parts of Cottonwood, including KNN
    This test is based on the Palmer Penguins study at
    https://gitlab.com/brohrer/study-knn-penguins
    """
    reports_dir = os.path.join("reports", "test_07")
    os.makedirs(reports_dir, exist_ok=True)
    figname = "penguin_miscategorizations.png"

    msg = f"""
Testing the KNN block.
  Look for documentation and visualizations
  in the {reports_dir} directory.
"""
    print(msg)

    # Create the k-NN model in Cottonwood.
    model = Structure()

    # Initialize the blocks.
    model.add(Data(), "data")
    model.add(KNN(), "knn")

    # Hook them up together.
    model.connect("data", "knn")
    model.connect("data", "knn", i_port_tail=1, i_port_head=1)

    misses = []
    for i_iter in range(int(model.blocks["data"].n_examples)):
        model.forward_pass()
        try:
            if (model.blocks["knn"].target_label ==
                    model.blocks["knn"].forward_out):
                misses.append(0)
            else:
                misses.append(1)
        except TypeError:
            # Initially some of the estimates are None. Ignore these
            pass

    misses = np.array(misses)
    accuracy = 1 - np.mean(misses)
    print(
        f"{np.sum(misses)} out of " +
        f"{misses.size} penguins misclassified " +
        f"for an accuracy of {accuracy * 100:.03} percent.")

    fig = plt.figure()
    ax = fig.gca()
    ax.plot(misses, color="#04253a")
    ax.set_title(f"k-NN with Palmer Penguins, k = {model.blocks['knn'].k}")
    ax.set_xlabel("Iteration")
    ax.set_ylabel("Missed penguins")
    ax.grid()
    fig_pathname = os.path.join(reports_dir, figname)
    plt.savefig(fig_pathname, dpi=300)


if __name__ == "__main__":
    run()
